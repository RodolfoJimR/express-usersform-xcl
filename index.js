const express = require("express");
const database = require("./config.js");
const port = 3000;
const app = express();
app.use(express.json());


app.listen(process.env.PORT || port, () => console.log("Escuchando en puerto 3000"));

app.get("/", (req, res) => {
    let mensaje = "Por favor, ingresa a la ruta /usuarios para acceder a la información";
    res.status(200).send(mensaje);
})
app.get("/usuarios", (req, res) => {
    database.ref(`Usuarios/`).once("value", (snapshot) => {
        let itemKey = [];
        snapshot.forEach((childSnapshot) => {
            const key = childSnapshot.key;
            const data = childSnapshot.val();
            itemKey.push({objectKey: key, ...data});
        });
        res.status(200).send(JSON.stringify(itemKey));
    });
});

app.get("/usuarios/:key", (req, res) => {
    database.ref(`Usuarios/${req.params.key}`).once("value", (snapshot) => {
        
        let all = snapshot.val();
        res.status(200).send(JSON.stringify(all));
    });
});

app.get("/usuarios/:key/nombre", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/`).once("value", (snapshot) => {
        
        let nombre = snapshot.val().nombre;
        res.status(200).send(JSON.stringify(nombre));
    });
});

app.get("/usuarios/:key/usuario", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/`).once("value", (snapshot) => {
        
        let usuario = snapshot.val().usuario;
        res.status(200).send(JSON.stringify(usuario));
    });
});

app.get("/usuarios/:key/n_form", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/`).once("value", (snapshot) => {
        
        let n_form = snapshot.val().n_form;
        res.status(200).send(JSON.stringify(n_form));
    });
});

app.get("/usuarios/:key/password", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/`).once("value", (snapshot) => {
        
        let password = snapshot.val().password;
        res.status(200).send(JSON.stringify(password));
    });
});

app.get("/usuarios/:key/Formularios", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios`).once("value", (snapshot) =>{
        let llaveForm = [];
        snapshot.forEach((childSnapshot) => {
            let keyForm = childSnapshot.key;
            let keyData = childSnapshot.val();
            llaveForm.push({numForm: keyForm, ...keyData});
        });
        res.status(200).send(JSON.stringify(llaveForm));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}`).once("value", (snapshot) => {
        let formularios = snapshot.val();
        res.status(200).send(JSON.stringify(formularios));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/descripcion", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/`).once("value", (snapshot) => {
        let descripcionForm = snapshot.val().descripcion;
        res.status(200).send(JSON.stringify(descripcionForm));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/id", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/`).once("value", (snapshot) => {
        let idForm = snapshot.val().id;
        res.status(200).send(JSON.stringify(idForm));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/id_origen", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/`).once("value", (snapshot) => {
        let idOrigenForm = snapshot.val().id_origen;
        res.status(200).send(JSON.stringify(idOrigenForm));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/id_usuario", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/`).once("value", (snapshot) => {
        let idUsuarioForm = snapshot.val().id_usuario;
        res.status(200).send(JSON.stringify(idUsuarioForm));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/id_usuario_origen", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/`).once("value", (snapshot) => {
        let idUsuarioOrigenForm = snapshot.val().id_usuario_origen;
        res.status(200).send(JSON.stringify(idUsuarioOrigenForm));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/titulo", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/`).once("value", (snapshot) => {
        let tituloForm = snapshot.val().titulo;
        res.status(200).send(JSON.stringify(tituloForm));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/preguntas", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/preguntas`).once("value", (snapshot) => {
        let llavePreg = [];
        snapshot.forEach((childSnapshot) => {
            let keyPreg = childSnapshot.key;
            let dataPreguntas = childSnapshot.val();
            llavePreg.push({numPreg: keyPreg, ...dataPreguntas});
        });
        res.status(200).send(JSON.stringify(llavePreg));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/preguntas/:keyPreg", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/preguntas/${req.params.keyPreg}`).once("value", (snapshot) => {
        let preguntasForm = snapshot.val();
        res.status(200).send(JSON.stringify(preguntasForm));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/preguntas/:keyPreg/id", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/preguntas/${req.params.keyPreg}`).once("value", (snapshot) => {
        let idPregunta = snapshot.val().id;
        res.status(200).send(JSON.stringify(idPregunta));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/preguntas/:keyPreg/pregunta", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/preguntas/${req.params.keyPreg}`).once("value", (snapshot) => {
        let dataPregunta = snapshot.val().pregunta;
        res.status(200).send(JSON.stringify(dataPregunta));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/preguntas/:keyPreg/tipo", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/preguntas/${req.params.keyPreg}`).once("value", (snapshot) => {
        let tipoPreg = snapshot.val().tipo;
        res.status(200).send(JSON.stringify(tipoPreg));
    });
});

app.get("/usuarios/:key/Formularios/:keyForm/preguntas/:keyPreg/opciones", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Formularios/${req.params.keyForm}/preguntas/${req.params.keyPreg}/opciones`).once("value", (snapshot) => {
        let opcionesPreg = snapshot.val();
        res.status(200).send(JSON.stringify(opcionesPreg));
    });
});

app.get("/usuarios/:key/Respuestas", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Respuestas`).once("value", (snapshot) => {
        let llaveResp = [];
        snapshot.forEach((childSnapshot) => {
            let keyResp = childSnapshot.key;
            let respData = childSnapshot.val();
            llaveResp.push({formID: keyResp, ...respData});
        });
        res.status(200).send(JSON.stringify(llaveResp));
    });
});

app.get("/usuarios/:key/Respuestas/:keyResp", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/Respuestas/${req.params.keyResp}`).once("value", (snapshot) => {
        let respObtenidas = snapshot.val();
        res.status(200).send(JSON.stringify(respObtenidas));
    });
});

app.get("/usuarios/:key/quien_responde", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/quien_responde/`).once("value", (snapshot) => {
        let llaveQuien = [];
        snapshot.forEach((childSnapshot) => {
            let keyQuien = childSnapshot.key;
            let quienData = childSnapshot.val();
            llaveQuien.push({quienID: keyQuien, ...quienData});
        });
        res.status(200).send(JSON.stringify(llaveQuien));
    });
});

app.get("/usuarios/:key/quien_responde/:keyQuien/", (req, res) => {
    database.ref(`Usuarios/${req.params.key}/quien_responde/${req.params.keyQuien}`).once("value", (snapshot) => {
        let collectionQuien = snapshot.val();
        res.status(200).send(JSON.stringify(collectionQuien));
    });
});


import express from 'express';

const router = express.Router();

// No repetir /usuarios, sería redundante, pues ya está en el index
router.get("/", (req, res)=> {
    res.send("Hola");
});

export default router;
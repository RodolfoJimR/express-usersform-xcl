const firebase = require("firebase");

const firebaseConfig = {
  apiKey: "AIzaSyCYMUiaE3ZzHSnZc2NYeShPFF1PNlT4uUs",
  authDomain: "dynamic-forms-87996.firebaseapp.com",
  databaseURL: "https://dynamic-forms-87996.firebaseio.com",
  projectId: "dynamic-forms-87996",
  storageBucket: "dynamic-forms-87996.appspot.com",
  messagingSenderId: "831911899167",
  appId: "1:831911899167:web:7ea7b4ce9ba558b6a0a52a",
  measurementId: "G-Z1NL5W726N"
};

firebase.initializeApp(firebaseConfig);

const database = firebase.database();
module.exports = database;